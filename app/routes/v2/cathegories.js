const express = require('express')
// para establecer las distintas rutas, necesitamos instanciar el express router
const router = express.Router()
const cathegoryController = require('../../controllers/v2/cathegoryController')

router.get('/', (req, res) => {
  //console.log('rutas de categorias')
  cathegoryController.index(req, res)
})
router.get('/search', (req, res) => {
  // console.log('cerveza encontrada')
  cathegoryController.search(req, res)
})

router.get('/:id', (req, res) => {
  
  cathegoryController.show(req, res)
})
router.post('/', (req, res) => {
  
  cathegoryController.create(req, res)
})
router.put('/:id', (req, res) => {
    cathegoryController.update(req, res)
})
module.exports = router