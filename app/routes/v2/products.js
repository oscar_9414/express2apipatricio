const express = require('express')
// para establecer las distintas rutas, necesitamos instanciar el express router
const router = express.Router()
const productController = require('../../controllers/v2/productController')

const auth = require('../../middlewares/auth')
//router.use(auth.auth)

router.get('/', auth.auth, (req, res) => {
  console.log('rutas de productos')
  productController.index(req, res)
})

router.get('/:id', (req, res) => {
  console.log('producto')
  productController.show(req, res)
})
router.post('/', (req, res) => {
  console.log('Crear un producto')
  productController.create(req, res)
})
router.put('/:id', (req, res) => {
  productController.update(req, res)
})

router.delete('/:id', (req, res) => {
  productController.destroy(req, res)
})
module.exports = router
