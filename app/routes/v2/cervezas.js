const express = require('express')
// para establecer las distintas rutas, necesitamos instanciar el express router
const router = express.Router()
const cervezaController = require('../../controllers/v2/cervezaController')

router.get('/', (req, res) => {
  console.log('rutas de cervezas2')
  cervezaController.index(req, res)
})
router.get('/search', (req, res) => {
  // console.log('cerveza encontrada')
  cervezaController.search(req, res)
})

router.get('/:id', (req, res) => {
  console.log('cerveza')
  cervezaController.show(req, res)
})
router.post('/', (req, res) => {
  console.log('Crear una cerveza')
  cervezaController.create(req, res)
})
router.put('/:id', (req, res) => {
  cervezaController.update(req, res)
})
module.exports = router
