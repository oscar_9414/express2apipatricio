const connection = require('../config/dbconnection.js')
const Cerveza = require('../models/Cerveza')

const index = function (req, res) {
  // conexion a la base de datos
  // console.log('lista de cervezas')
  const cervezas = Cerveza.index(function (status, data, fields) {
    res.status(status).json(data)
  })
  // console.log(cervezas)
  // console.log("lista de cervezas");
  // res.json({ mensaje: `¡Lista de cervezas` })
  //   return 'Lista de cervezas desde el controlador'
}

const show = function (req, res) {
  console.log('Detalle de cerveza')
  const id = req.params.id
  const cerveza = Cerveza.find(id, function (status, data) {
    res.status(status).json(data)
    // res.json({ mensaje: `¡Cerveza ${id}` })
    //   return 'Lista de cervezas desde el controlador'
  })
}
const store = function (req, res) {
  console.log('Crear una nueva cervezas')
  const cerveza = {
    name: req.body.name,
    description: req.body.description,
    price: req.body.price,
    alcohol: req.body.alcohol
  }
  Cerveza.create(cerveza, (err, data) => {
    if (err) {
      res.status(500).json({ mensaje: 'falta la inserccion' })
    } else {
      res.status(201).json(data)
    }
  })

  // res.json({ mensaje: `¡Cerveza ${req.body.nombre}` })
  //   return 'Lista de cervezas desde el controlador'
}
const update = function (req, res) {
  const cerveza = {
    id: req.params.id,
    name: req.body.name,
    container: req.body.description,
    price: req.body.price,
    alcohol: req.body.alcohol
  }
  Cerveza.update(cerveza, (err, data) => {
    if (err) {
      res.status(500).json(err)
    } else {
      res.json(res)
    }
  })
  // res.json({ mensaje: `actualizar ${id}` })
  //   return 'Lista de cervezas desde el controlador'
}
const destroy = function (req, res) {
  const id = req.params.id
  Cerveza.destroy(id, (err, data) => {
    if (err) {
      res.status(err).json({ mensaje: 'borrado' })
    } else {
      res.status(err).json(data)
    }
  })
  // res.json({ mensaje: ` borrar ${id}` })
  //   return 'Lista de cervezas desde el controlador'
}
module.exports = {
  index,
  show,
  store,
  update,
  destroy
}
